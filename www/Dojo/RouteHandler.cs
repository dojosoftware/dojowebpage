﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Compilation;
using System.Web.UI;

namespace Dojo
{
    public class LangRouteHandler : IRouteHandler
    {
        string langConst;

        public LangRouteHandler(string langConst)
        {
            this.langConst = langConst;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            string path = (requestContext.RouteData.Values["path"] as string);

            if (string.IsNullOrEmpty(path))
            {
                // This should not happen. When there is no culture specified in the path
                // (e.g. http://www.dojo-software.com/Foo.aspx) then this method should
                // not be run. It should only intercept requests when culture is defined.
                return BuildManager.CreateInstanceFromVirtualPath("/Default.aspx", typeof(Page)) as Page;
            }
            else
            {
                HttpContext.Current.Items[Const.langParam] = langConst;

                return BuildManager.CreateInstanceFromVirtualPath("~/" + path, typeof(Page)) as Page;
            }
        }
    }
}