﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dojo
{
    public partial class Contact : PageBase
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            var email = "contact@dojo-software.com";
            email = string.Format("<a href='mailto:{0}'>{0}</a>", email);

            intro.InnerHtml = Resources.Dojo.ContIntro;
            contactus.InnerHtml = Resources.Dojo.ContTitle;
            officeinfo.InnerHtml = Resources.Dojo.ContOfficeInfo;
            phonenr.InnerHtml = Resources.Dojo.ContOfficePhoneNr;
            countryname.InnerHtml = Resources.Dojo.ConfOfficeCountryName;

            demotitle.InnerHtml = Resources.Dojo.ContDemoTitle;
            demotext.InnerHtml = Resources.Dojo.ContDemoText;
        }

        public override bool ContactSelected { get { return true; } }
        public override string MenuTitle { get { return Resources.Dojo.MenuContact; } }
        public override string PageDescription { get { return Resources.Dojo.MetaContact; } }
    }
}