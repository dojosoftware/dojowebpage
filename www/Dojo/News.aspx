﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="Dojo.News" %>


<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="server">

<div id="newsfull" runat="server" class="contblock spacer substatement">
    <div id="newsdate" runat="server" class="newsdate">(resx) Date of news</div>
    <div id="newstext" runat="server">(resx) Full news, if any..</div>
    <div class="backtohomepage"><a href="Default.aspx"><span id="goback" runat="server">Back to home page</span></a></div>
</div>

</asp:Content>
