﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dojo
{
    public static class Const
    {
        public const string contextInstance = "session_context_instance";
        public const string sessionExists = "session_exists";
        public const string langParam = "new_culture";
        public const string langCookie = "user_culture";
    }
}