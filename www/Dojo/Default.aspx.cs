﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Dojo
{
    public partial class DefaultPage : PageBase
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            splash.InnerHtml = RenderSplashImages();
            mainstatement.InnerHtml = Resources.Dojo.MainStatement;

            intro.InnerHtml = Resources.Dojo.TheriakIntroduction;
            RenderNewsIfAny();
        }

        string RenderSplashImages()
        {
            var imgs = new StringBuilder();
            var img = "<img src='/images/splash{0}.jpg' alt='' />";
            var splashCount = GetSplashCount();
            var filenames = new List<int>();

            for (int i = 1; i <= splashCount; i++) filenames.Add(i);

            // Now the list is ordered 1...N. Next we randomize it so we won't always get the same sequence.
            filenames = filenames.OrderBy(x => Guid.NewGuid()).ToList();

            foreach (int filenr in filenames) imgs.AppendFormat(img, filenr);

            return imgs.ToString();
        }

        private int GetSplashCount()
        {
            int count = 0;

            while (System.IO.File.Exists(Request.PhysicalApplicationPath + "images\\splash" + (count + 1) + ".jpg")) count++;

            return count;
        }

        void RenderNewsIfAny()
        {
            if (Resources.Dojo.NewsIntro.Trim() == "")
            {
                news.Visible = false;
            }
            else
            {
                news.Visible = true;
                newsintro.InnerHtml = Resources.Dojo.NewsIntro;
                readmorenews.InnerHtml = Resources.Dojo.NewsReadMore;
            }
        }

        public override bool HomeSelected { get { return true; } }
        public override string MenuTitle { get { return Resources.Dojo.MenuHome; } }
        public override string PageDescription { get { return Resources.Dojo.MetaHome; } }
    }
}
