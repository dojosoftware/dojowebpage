﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Globalization;

namespace Dojo
{
    public static class MenuRenderer
    {
        public static string RenderMenu(PageBase page, HttpRequest request)
        {
            var html = new StringBuilder();
            var divItem = "<div class='{0}'><a class='{1}' href='{2}'>{3}</a></div>";
            var mnusel = "menuitem_selected";
            var mnuit = "menuitem";
            var ml = "menulink";

            html.AppendFormat(divItem, (page.HomeSelected ? mnusel : mnuit), (page.HomeSelected ? "" : ml), "Default.aspx", Resources.Dojo.MenuHome);
            html.AppendFormat(divItem, (page.ContactSelected ? mnusel : mnuit), (page.ContactSelected ? "" : ml), "Contact.aspx", Resources.Dojo.MenuContact);

            return html.ToString();
        }

    }
}