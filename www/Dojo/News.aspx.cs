﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dojo
{
    public partial class News : PageBase
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            var newstxt = Resources.Dojo.NewsFull;
            var newsdt = Resources.Dojo.NewsDate;

            if (string.IsNullOrEmpty(newstxt))
            {
                newsdate.Visible = false;
                newstext.InnerHtml = "No current news...";
            }
            else
            {
                if (string.IsNullOrEmpty(newsdt))
                {
                    newsdate.Visible = false;
                }
                else
                {
                    newsdate.InnerHtml = newsdt;
                }

                newstext.InnerHtml = newstxt;
            }

            goback.InnerHtml = Resources.Dojo.NewsGoBack;
        }

        public override string MenuTitle { get { return Resources.Dojo.MenuHome; } }
        public override string PageDescription { get { return Resources.Dojo.MetaHome; } }
    }
}