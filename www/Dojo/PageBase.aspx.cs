﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.IO;

namespace Dojo
{
    public partial class PageBase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.GetType().ToString() == "ASP.pagebase_aspx") Response.Redirect("Default.aspx");
        }

        protected override void InitializeCulture()
        {
            string lang = GetCultureConst();

            if (Culture != lang)
            {
                UICulture = lang;
                Culture = lang;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);

                Response.Cookies[Const.langCookie].Value = lang;
                Response.Cookies[Const.langCookie].Expires = DateTime.Now.AddMonths(1);
            }

            base.InitializeCulture();
        }

        string GetCultureConst()
        {
            string cult = (string)HttpContext.Current.Items[Const.langParam];

            if (string.IsNullOrEmpty(cult))
            {
                // The user has no specified culture in the path. 
                // 
                // http://www.dojo-software.com/Foo.aspx
                //
                // That is ok. By definition, this means that the "en-US" culture is active.
                // That could of course be overridden by a stored language.
                //
                if (Request.Cookies[Const.langCookie] != null)
                {
                    string storedCult = Request.Cookies[Const.langCookie].Value;

                }

                // A default fallback. No culture defined in path, no culture in cookies.

                return "en-US";
            }
            else
            {
                HttpContext.Current.Items[Const.langParam] = "";
                return cult;
            }
        }

        /// <summary>Returns true if the "Home" page is selected, false otherwise.</summary>
        public virtual bool HomeSelected { get { return false; } }
        /// <summary>Returns true if the "Contact" page is selected, false otherwise.</summary>
        public virtual bool ContactSelected { get { return false; } }

        /// <summary>The title of the page in the menu.</summary>
        public virtual string MenuTitle { get { return ""; } }
        /// <summary>Used for the meta tag "description".</summary>
        public virtual string PageDescription { get { return ""; } }
    }
}