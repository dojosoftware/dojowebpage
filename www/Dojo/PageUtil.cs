﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

namespace Dojo
{
    public static class PageUtil
    {
        public static int? ReadParam(System.Web.UI.Page page, string paramName)
        {
            if (page.Request.Params[paramName] != null)
            {
                int value;

                if (int.TryParse(page.Request.Params[paramName], out value)) return value;
            }

            return null;
        }
    }
}