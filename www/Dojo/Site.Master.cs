﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using System.IO;
using System.Web.UI.HtmlControls;

namespace Dojo
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var title = new StringBuilder();
            var mt = (Page as PageBase).MenuTitle;
            var titleLocation = mt.Substring(0, 1).ToUpper() + mt.Substring(1);

            title.Append("Dojo Software");
            title.Append(" | ");
            title.Append("Committed to medication safety"); // Ok to have it like this while we are not translating the slogan.
            title.Append(" | ");
            title.Append(titleLocation);

            Page.Header.Title = title.ToString();
            InsertMetaTags();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            menu.InnerHtml = MenuRenderer.RenderMenu(Page as PageBase, Request);
            flags.InnerHtml = RenderLanguageSelector();
        }

        string RenderLanguageSelector()
        {
            var filename = Path.GetFileName(Request.PhysicalPath);
            var url = string.Format("<a href='/{0}/{1}'>", "{0}", filename);
            var flag = "<span class='flag {0}'>{1}<img src='/images/flag/flag-{2}w.jpg' alt='' border='0' />{3}</span>";
            var selclass = "flagselected";

            // Status: filename is the filename of the current url, i.e. if full url is
            // http://www.foo.com/en-US/Foo.aspx, then filename is Foo.aspx.

            var cultdk = (CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "da");
            var urlbdk = cultdk ? "" : string.Format(url, "da-DK");
            var urledk = cultdk ? "" : "</a>";
            var flagdk = string.Format(flag, cultdk ? selclass : "", urlbdk, "dk", urledk);

            var cultis = (CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "is");
            var urlbis = cultis ? "" : string.Format(url, "is-IS");
            var urleis = cultis ? "" : "</a>";
            var flagis = string.Format(flag, cultis ? selclass : "", urlbis, "is", urleis);
            
            // If English -OR- any unsupported culture is selected (via the URL) then 
            // we fallback to English and therefore select the English flag.
            var culten = /*!cultDk &&*/ !cultis;
            var urlben = culten ? "" : string.Format(url, "en-US");
            var urlene = culten ? "" : "</a>";
            var flagen = string.Format(flag, culten ? selclass : "", urlben, "gb", urlene);

            var flags = new StringBuilder();

            flags.Append("<img src='/images/leftofflag.gif' alt='' />");
            flags.Append(flagen);
            //flags.Append(fdk);
            flags.Append(flagis);

            return flags.ToString();
        }

        public void InsertMetaTags()
        {
            HtmlMeta tag = new HtmlMeta();
            tag.Name = "description";
            tag.Content = (Page as PageBase).PageDescription;
            Page.Header.Controls.Add(tag);
        }

    }
}
