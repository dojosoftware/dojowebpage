//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Dojo {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Dojo() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Dojo", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Iceland.
        /// </summary>
        internal static string ConfOfficeCountryName {
            get {
                return ResourceManager.GetString("ConfOfficeCountryName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Interested to get a presentation of our products or hear about our services? We will be pleased to arrange a demo and investigate with you how you can benefit from a cooperation with us. Please contact us on mail or phone:&lt;ul&gt;&lt;li&gt;Via email – &lt;a href=&quot;mailto:contact@dojo-software.com&quot;&gt;contact@dojo-software.com&lt;/a&gt;&lt;/li&gt;&lt;li&gt;Skúli – +354 690 2348&lt;/li&gt;&lt;li&gt;Ellen – +45 4040 1997&lt;/li&gt;&lt;/ul&gt;.
        /// </summary>
        internal static string ContDemoText {
            get {
                return ResourceManager.GetString("ContDemoText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Demo.
        /// </summary>
        internal static string ContDemoTitle {
            get {
                return ResourceManager.GetString("ContDemoTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We are Dojo Software ApS, and we have our office in Akureyri, Iceland. Our Danish CVR number is DK34203466, and in Iceland we&apos;re Dojo Software ApS, útibú á Íslandi kt. 530716-0580..
        /// </summary>
        internal static string ContIntro {
            get {
                return ResourceManager.GetString("ContIntro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please call us at office hours (8-16 Icelandic time).
        /// </summary>
        internal static string ContOfficeInfo {
            get {
                return ResourceManager.GetString("ContOfficeInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to +354 690 2348.
        /// </summary>
        internal static string ContOfficePhoneNr {
            get {
                return ResourceManager.GetString("ContOfficePhoneNr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact us.
        /// </summary>
        internal static string ContTitle {
            get {
                return ResourceManager.GetString("ContTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At Dojo Software, we want to improve quality of life for patients and clinical users of medication-software in the healthcare sector..
        /// </summary>
        internal static string MainStatement {
            get {
                return ResourceManager.GetString("MainStatement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to contact.
        /// </summary>
        internal static string MenuContact {
            get {
                return ResourceManager.GetString("MenuContact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to home.
        /// </summary>
        internal static string MenuHome {
            get {
                return ResourceManager.GetString("MenuHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dojo Software deals with medication safety in hospitals. There are a few ways of reaching us, please find the way that suits you best..
        /// </summary>
        internal static string MetaContact {
            get {
                return ResourceManager.GetString("MetaContact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dojo Software deals with medication safety in hospitals. This page contains an introduction to us and the concept of &quot;dojo&quot;..
        /// </summary>
        internal static string MetaHome {
            get {
                return ResourceManager.GetString("MetaHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to December 1st, 2013.
        /// </summary>
        internal static string NewsDate {
            get {
                return ResourceManager.GetString("NewsDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  .
        /// </summary>
        internal static string NewsFull {
            get {
                return ResourceManager.GetString("NewsFull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go back to home page.
        /// </summary>
        internal static string NewsGoBack {
            get {
                return ResourceManager.GetString("NewsGoBack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  .
        /// </summary>
        internal static string NewsIntro {
            get {
                return ResourceManager.GetString("NewsIntro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Read the news.
        /// </summary>
        internal static string NewsReadMore {
            get {
                return ResourceManager.GetString("NewsReadMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;Therapy&lt;/strong&gt; is a &lt;span class=&quot;keywords&quot;&gt;feature complete &lt;strong&gt;medication management system&lt;/strong&gt;&lt;/span&gt; that has supported hospitals in optimizing their clinical and logistical processes for almost 20 years. &lt;br /&gt;&lt;br /&gt;&lt;strong&gt;Therapy&lt;/strong&gt; &lt;span class=&quot;keywords&quot;&gt;integrates to various EPJ&lt;/span&gt; and pharmacy stock systems and is a cornerstone in &lt;strong&gt;mission critical&lt;/strong&gt; solutions that hospitals are providing today.  It has been used in 6 European countries and their wide rang [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TheriakIntroduction {
            get {
                return ResourceManager.GetString("TheriakIntroduction", resourceCulture);
            }
        }
    }
}
