﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Dojo.Contact" %>


<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="server">

<div class="contblock">

<div id="intro" runat="server" class="contact_section" style="xpadding-bottom: 20px;">(resx) We are Dojo Software ...</div>

<h2 id="contactus" class="contact_header" runat="server">(resx)Contact us...</h2>

<div class="contact_section">
<table style="width:100%;">
    <tr>
        <td style="width:40%;">Skúli Jóhannesson</td>
        <td style="width:60%;"><div id="officeinfo" runat="server" style="xpadding-left: 130px;">(resx)Call us...</div></td>
    </tr>
    <tr>
        <td style="width:40%;">Skipagata 14, 4. hæð</td>
        <td style="width:60%;"><div id="phonenr" runat="server">(resx) 999 9999</div></td>
    </tr>
    <tr>
        <td style="width:40%;">IS-600 Akureyri</td>
        <td style="width:60%;"><a href="mailto:contact@dojo-software.com">contact@dojo-software.com</a></td>
    </tr>
    <tr>
        <td style="width:40%;"><div id="countryname" runat="server">(resx) Iceland</div></td>
        <td style="width:60%;"></td>
    </tr>
</table>
</div>

<h2 id="demotitle" class="contact_header" runat="server">(resx)Demo...</h2>

<div class="contact_section">
    <div id="demotext" style="float:left; clear:both; width:50%" runat="server" >(resx)Interested in demo...</div>
    <div id="imgdemo" style="float:right;"><img src="/images/demo.jpg" alt="Therapy demo" style="margin-bottom: 20px;" /></div>
</div>

</div>

</asp:Content>
