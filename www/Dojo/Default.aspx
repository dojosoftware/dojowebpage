﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Dojo.DefaultPage" %>

<asp:Content ID="cntJs" runat="server" ContentPlaceHolderID="JsContent">
<script type="text/javascript" src="/scripts/jquery.cycle.lite.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('.splash').cycle({
            fx: 'fade',
            timeout: 10000,
            random: 1
        });

        $('.showmore').click(function () {
            $(this).hide();
            $dots = $(this).prevAll('.minoritemintrodots').eq(0);
            $dots.hide();
            $full = $(this).nextAll('.minoritemfull').eq(0);
            $full.fadeIn('slow');
        });

        $('.showless').click(function () {
            $showmore = $(this).parent().prevAll('.showmore').eq(0);
            $dots = $(this).parent().prevAll('.minoritemintrodots').eq(0);
            $dots.show();
            $showmore.show();
            $(this).parent().fadeOut('slow');
        });
    });
    
</script>

</asp:Content>

<asp:Content ID="cntMain" runat="server" ContentPlaceHolderID="MainContent">

<div class="contblock">
    <table width="100%">
        <tr>
            <td valign="top"><div id="mainstatement" runat="server" class="mainstatement">(resx) At Dojo Software, we want to improve quality of life for patients and ...</div></td>
            <td valign="top" width="246px"><div id="splash" runat="server" class="splash" xstyle="border: solid blue 1px;"></div></td>
        </tr>
    </table>
</div>

<div id="news" runat="server" class="contblock spacer substatement newsintroblock" >
<table>
    <tr>
        <td><img src="/images/lightbulb.gif" alt="" /></td>
        <td style="padding-left: 10px;">
            <span id="newsintro" runat="server" class="newsintrotext">(resx) The latest news, if any..</span>
            <span class='showmore showmoreorless morenews'><a href="News.aspx"><span id="readmorenews" runat="server">Read more</span></a></span>
        </td>
    </tr>
</table>
</div>

<div id="intro" runat="server" class="contblock spacer substatement">(resx) Theriak is a feature complete..</div>

</asp:Content>
