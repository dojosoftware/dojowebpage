﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Dojo.demo.Default1" %>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="refresh" content="0; URL='/Contact.aspx'">
    <meta name="keywords" content="automatic redirection">
</head>
<body>
    If your browser doesn't automatically go there within a few seconds,
    you may want to go to
    <a href="/Demo.aspx">the demo page</a>
    manually.
</body>
</html>
