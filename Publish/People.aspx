﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="People.aspx.cs" Inherits="Dojo.People" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="MainContent" runat="server">

<div class="contblock">
<div id="introduction" runat="server" style="display: none;">(resx: EmpIntroduction)</div>
<div>
<asp:Repeater runat="server" ID="employees">
<HeaderTemplate>
    <table class="ppl_list">
</HeaderTemplate>
<ItemTemplate>
    <tr>
        <td><div class="ppl_margin_left"></div></td>
        <td valign="top"><img class="ppl_img" src="/images/<%# DataBinder.Eval(Container.DataItem, "ImgName") %>.jpg" 
            alt="<%# DataBinder.Eval(Container.DataItem, "Name") %>, <%# DataBinder.Eval(Container.DataItem, "JobTitle") %>, Dojo Software" /></td>
        <td valign="top">

            <span class='minoritemtitle'><%# DataBinder.Eval(Container.DataItem, "Name") %>, 
                <%# DataBinder.Eval(Container.DataItem, "JobTitle") %> </span>
            <%# DataBinder.Eval(Container.DataItem, "About") %>
        </td>
    </tr>
    <tr><td colspan="3"><div style="height: 35px;"</td></tr>
</ItemTemplate>
<FooterTemplate>
    </table>
</FooterTemplate>
</asp:Repeater>
</div>

<div>

<div class="ppl_separator"></div>
<div>
<asp:Repeater runat="server" ID="cofounders">
<HeaderTemplate>
    <table class="ppl_list">
        <tr>
            <td></td>
            <td></td>            
            <td><div id="additionsupport" runat="server" onprerender="divAdditionSupport_PreRender">(resx: SupportAssistanceCofounders)</div></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td><div class="ppl_margin_left"></div></td>
            <td valign="top"><img class="ppl_img" src="/images/cofounders.jpg" alt="" /></td>
            <td>
</HeaderTemplate>
<ItemTemplate>
                <div><span class='minoritemtitle'><%# DataBinder.Eval(Container.DataItem, "Name") %></span> 
                    <%# DataBinder.Eval(Container.DataItem, "About") %>
                </div>
                <div>&nbsp;</div>
</ItemTemplate>
<FooterTemplate>
            </td>
        </tr>
    </table>
</FooterTemplate>
</asp:Repeater>
</div>
</div>

</asp:Content>
