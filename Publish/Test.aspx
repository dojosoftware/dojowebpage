﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Dojo.Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Testing</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</head>
<body>
    <!-- orange header line -->
    <div style="height: 10px; background-color: #ff7e02;"></div>

    <!-- The center block -->
    <div id="container" xstyle="position: relative;">
        <div style="background-color: blue;"><img src="images/toplogo.jpg" alt="" /><img src="images/topsep.gif" alt=""
            width="190px" height="70px" /><img src="images/topslogan.jpg" alt="" 
            style="position: relative; left: 0px; top: 16px;" /></div>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="menubarlayout"></td>
                    <td><div style="height: 50px;"></div></td>
                </tr>
                <tr>
                    <td class="menubarlayout" valign="top">
                        <div>home</div>
                        <div>people</div>
                        <div>contact</div>
                    </td>
                    <td valign="top">
                        <div>All text.</div>
                    </td>
                </tr>
                <tr>
                    <td class="menubarlayout"></td>
                    <td><div style="position: relative; top: 11px; text-align: right;"><img 
                        src="images/flag/flag-gb.png" alt="" /><img src="images/flag/flag-dk.png" 
                        alt="" /><img src="images/flag/flag-is.png" alt="" /></div></td>
                </tr>
            </table>
        </div>

        <div style="height: 1px; background-color: #5f5f5f;"></div>
    </div>
</body>
</html>
